using NUnit.Framework;
using FluentAssertions;

namespace api.tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Get_Content_Returns_String(){
            ContentController contentController = new();
            contentController.GetContent().Should().Be("Hello World");
        }

    }
}