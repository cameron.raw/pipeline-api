using System;
using System.Net;
using System.Text;
using System.Threading;

namespace api
{
    public class Server
    {
        private static int PORT = 5000;
        private static readonly HttpListener _httpListener = new();

        public void Start()
        {
            Console.WriteLine("Server starting...");
            _httpListener.Prefixes.Add($"http://localhost:{PORT}/");
            _httpListener.Prefixes.Add($"http://localhost:{PORT}/test/");
            _httpListener.Start();
            Console.WriteLine("Server started!");
            var _responseThread = new Thread(ResponseThread);
            _responseThread.Start();
        }

        private static void ResponseThread()
        {
            ContentController contentController = new();
            while (true)
            {
                var context = _httpListener.GetContext(); // get a context
                // Now, you'll find the request URL in context.Request.Url
                var _responseArray = Encoding.UTF8.GetBytes(contentController.GetContent()); // get the bytes to response
                context.Response.OutputStream.Write(_responseArray, 0,
                    _responseArray.Length); // write bytes to the output stream
                context.Response.KeepAlive = false; // set the KeepAlive bool to false
                context.Response.Close(); // close the connection
                Console.WriteLine("Response given to a request.");
            }
        }
    }
}